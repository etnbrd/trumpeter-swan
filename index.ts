import * as path from 'path'
import { Project, ts, Type } from 'ts-morph'

const projectRootPath = '/Users/etienne.brodu/dd/synthetics-worker'
const project = new Project({
  tsConfigFilePath: path.join(projectRootPath, 'worker/tsconfig.json'),
});

async function main() {

  // const diagnostics = project.getPreEmitDiagnostics();
  // console.log(diagnostics)

  // const classes = project.getSourceFiles().flatMap(sourceFile =>
  //   sourceFile.getClasses().map(classDeclaration => ({
  //     sourceFile: sourceFile.getFilePath(),
  //     name: classDeclaration.getName()
  //   }))
  // )
  // console.log(classes)

  const imports = project.getSourceFiles().map(sourceFile => {
    const importDeclarations = sourceFile.getImportDeclarations().map(importDeclaration => {

      const imports = []
      const defaultImport = importDeclaration.getDefaultImport()
      if (defaultImport) {
        imports.push(defaultImport.getText())
      }

      const namespaceImport = importDeclaration.getNamespaceImport()
      if (namespaceImport) {
        imports.push(namespaceImport.getText())
      }

      const namedImports = importDeclaration.getNamedImports()
      if (namedImports) {
        imports.push(...namedImports.map(namedImport => namedImport.getText()))
      }

      return {
        moduleSpecifier: importDeclaration.getModuleSpecifierValue(),
        imports
      }
    })


    return {
      sourceFile: sourceFile.getFilePath(),
      imports: importDeclarations,
    }
  })

  for (const importDeclaration of imports) {
    console.log(importDeclaration.sourceFile)
    console.log(importDeclaration.imports)
    console.log('---')
  }
}

main()